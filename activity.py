# Specification
#  1. Create an abstract class called Animal that has the following abstract methods
#     Abstract Methods: eat(food), make_sound()
#  2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:
#     Properties:
#        name, breed, age
#    Methods:
#       getters and setters, implementation of abstract methods, call()

# Item No. 1
from abc import ABC, abstractclassmethod


class Animal(ABC):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def set_name(self, name):
        self._name = name

    def get_name(self):
        print(f"Name is: {self._name}")

    def set_breed(self, breed):
        self._breed = breed

    def get_breed(self):
        print(f"Breed is: {self._breed}")

    def set_age(self, age):
        self._age = age

    def get_age(self):
        print(f"Age is: {self._age}")

    @abstractclassmethod
    def make_sound(self):
        pass

    def eat(food):
        pass

    def call(self):
        pass


class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__

    def make_sound(self):
        print("Bark! Woof! Arf!")

    def eat(self, food):
        print(f"Eaten {food}")

    def call(self):
        print(f"Here Isis!")


class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__

    def make_sound(self):
        print("Miaow! Nyaw! Nyaaaaa!")

    def eat(self, food):
        print(f"Serve me {food}")

    def call(self):
        print(f"Puss, come on!")


# Test Cases:
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()
